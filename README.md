API test task
===========

## Task description:
**Create API tests for CertificatesList endpoint using folowing test-cases**

Use this public repository for finished task. If you unable to create PR to repo, use fork or ask for access.  
Use **Python** for writing tests.  
You should create new project from a scratch. Project should be in the new branch. Create new Pull Request when task will be finished.
Project should contain covered test cases that are described in README and contain instructions how to build and run your project.

## Application credentials:
	Home page - https://www.sbzend.ssls.com
	Email for login - ssls.automation+99@gmail.com
	Password for login - 123456

#### Preconditions:
1. Authorize via API
* Send request to `https://www.sbzend.ssls.com`
* Get `sslsSESSID` cookie value

2.	Send requset to `https://www.sbzend.ssls.com/authorize/authenticate` with cookie `sslSESSID` and body:
		`{
			email: ${email},
			password: ${password}
		}`

#### Steps to reproduce:
Get list of user certificates from `https://www.sbzend.ssls.com/certificates/list` endpoint

#### Expected results:
1. Only 1 certificate returned
2. `'id'` and `'userId'` parameters are numeric and present
3. Returned values in `'info'` object of response should be equal to data returned by endpoint `https://www.sbzend.ssls.com/api/products/comodo-${certificateType}`.

`'certificateType'` - value of `'type'` parameter from `https://www.sbzend.ssls.com/certificates/list` enpoint

Use following mapping for checking

certificates/list      | products/comodo-${certificateType}
:--------------------: | :---------------------------------:
  name			       | name
  brand 			   | brand
  isWildcard           | isWildcard
  validation  	       | validationType
  sansIncluded	       | includedSans
  type                 | certType
